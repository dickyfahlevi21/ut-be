require("dotenv").config();
const cloudinary = require("cloudinary").v2;
const cloudConfig = require("../config/cloudinaryConfig");
cloudinary.config(cloudConfig);

const { Product, Admin, Category, Variation } = require("../models");
const response = require("../helpers/response");
const pagination = require("../helpers/pagination");

// const attProduct = ["id", "name", "photo_url", "stock", "price"];
const attProduct = ["id", "name", "price", "weight", "description", "tags", "stock", "picture", "publicId"];
const attAdmin = ['id', 'firstName', 'lastName', 'email', 'phoneNumber' , 'username']
const attCategory = ['id', 'categoryName']
const attVariation = ['id', 'color', 'size', 'material']

class ProductController {
  // uji coba routes berjalan dengan baik
  static async home(req, res) {
    return res
      .status(200)
      .json(
        response("Success", "Sukses akses!", "Hai from Product Controller")
      );
  }

  //Done
  static async getProduct(req, res) {
    const { id } = req.params;

    const productget = await Product.findByPk(id, {
      attributes: attProduct,
      include: [
        {
          model: Admin,
          attributes: attAdmin,
        },
        {
          model: Category,
          attributes: attCategory,
        },
        {
          model: Variation,
          attributes: attVariation,
        },
      ],
    });

    try {
      if (productget) {
        return res
          .status(200)
          .json(response("Success", "Data product ditemukan!", productget));
      } else {
        return res
          .status(400)
          .json(response("Failed!", "Data product tidak ditemukan!", ""));
      }
    } catch (error) {
      return res.status(404).json(response("Failed", error.message, ""));
    }
  }

  // Done
  static async getProducts(req, res) {
    try {
      // console.log(req.user_id);
      const count = await Product.count();

      const page = pagination({
        limit: req.query.limit,
        page: parseInt(req.query.page),
        count: count,
      });

      const show = await Product.findAll({
        limit: page.limit,
        offset: page.offset,
        attributes: attProduct,
        include: [
          {
            model: Admin,
            attributes: attAdmin,
          },
          {
            model: Category,
            attributes: attCategory,
          },
          {
            model: Variation,
            attributes: attVariation,
          },
        ],
      });

      const datas = {
        data: show,
        totalItems: page.totalItems,
        totalPages: page.totalPages,
        currentPage: page.currentPage,
      };
      res.status(200).json(response("Success", "Berhasil tampil data", datas));
    } catch (err) {
      return res.status(400).json(response("Failed", err));
    }
  }

  // Done
  static async saveProduct(req, res) {
    try {
      if(req.admin_id){
        const { name, price, weight, description, tags, stock, categoryName, color, size, material} = req.body;

        const idAdmin = req.admin_id;

        // ambil file dari body request
        const uploadFile = req.files.photo;

        // upload pakai cloudinary
        const image = await cloudinary.uploader.upload(
          uploadFile.tempFilePath,
          (err, result) => {
            console.log({ err, result });
            if (result === undefined) {
              throw new Error(`Cloudinary Error:${err.message}`);
            }
          }
        );

        // tambah product
        const saveProduct = await Product.create({
          name,
          price,
          weight,
          description,
          tags,
          stock,
          picture: image.secure_url,
          publicId: image.public_id,
          idAdmin,
        });
        // console.log(saveProduct);

        // tambah kategori
        const saveCategory = await Category.create({
          idProduct: saveProduct.id,
          categoryName
        })

        // tambah variasi
        const saveVariation = await Variation.create({
          idProduct: saveProduct.id,
          color,
          size,
          material
        })

        const datas = {
          "Nama Product": saveProduct.name,
          idAdmin:saveProduct.idAdmin,
          price:saveProduct.price,
          weight:saveProduct.weight,
          description:saveProduct.description,
          tags:saveProduct.tags,
          stock:saveProduct.stock,
          picture:saveProduct.picture,
          publicId:saveProduct.publicId,
          categoryName:saveCategory.categoryName,
          color:saveVariation.color,
          size:saveVariation.size,
          material:saveVariation.material
        };

        res
          .status(201)
          .json(response("Success", "Berhasil tambah data product", datas));
      }
      else {
        res.status(404).json(response("Failed", "You don't have access"));
      }
    } catch (error) {
      res.status(404).json(response("Failed", error.message));
    }
  }

  static async updateProduct(req, res) {
    try {
      if (req.admin_id) {
        const { id } = req.params;
        const { name, price, weight, description, tags, stock, categoryName, color, size, material} = req.body;
  
        // ambil file dari body request
        // const uploadFile = req.files.photo;

        if (req.files) {
          const findProduct = await Product.findByPk(id, {
            attributes: attProduct,
            include: [
              {
                model: Admin,
                attributes: attAdmin,
              },
            ],
          });
          
          await cloudinary.uploader.destroy(findProduct.publicId,
            function (err, res) {
              if (err) {
                throw new Error(`Failed deleting cloudinary image :${err.message}`);
              }
          })

          // upload pakai cloudinary
          const image = await cloudinary.uploader.upload(
            req.files.photo.tempFilePath,
            (err, result) => {
              console.log({ err, result });
              if (result === undefined) {
                throw new Error(`Cloudinary Error:${err.message}`);
              }
            }
          );
          console.log(image);

          await Product.update({
              publicId: image.public_id,
              picture: image.secure_url
            }, {
              where: { id }
          })
        }
        // update product
        await Product.update({
          name,
          price,
          weight,
          description,
          tags,
          stock
        },
        {
          where: {
            id,
          },
        });

        await Category.update({
          categoryName
        },
        {
          where: {
            productId: id,
          },
        });

        await Variation.update({
          color,
          size,
          material
        },
        {
          where: {
            productId: id,
          },
        });

        return res
          .status(200)
          .json(response("Success", "Sukses update data produk!"));
      } else {
        res.status(404).json(response("Failed", "You don't have access"));
      }
      
      } catch (error) {
        res.status(404).json(response("Failed", error.message));
      }
  }

  // Done
  static async deleteProduct(req, res) {
      try {
        const { id } = req.params;

      const findProduct = await Product.findByPk(id, {
        attributes: attProduct,
        include: [
          {
            model: Admin,
            attributes: attAdmin,
          },
        ],
      });
        // console.log(findProduct);

        const product = await Product.destroy({
            where: {
                id: req.params.id
            }
        })

        const deleteImg = await cloudinary.uploader.destroy(findProduct.publicId,
          function (err, res) {
            if (err) {
              throw new Error(`Failed deleting cloudinary image :${err.message}`);
            }
          })

        if(!product || !deleteImg) {
            throw new Error(error.message)
        }
        
        return res
            .status(200)
            .json(response('Success', 'Product has been deleted'))

    } catch (error) {
        return res
            .status(500)
            .json(response('Fail', error.message)   )
    }
  }

  // Done
  static async deleteProductAll (req, res){
    try {
      const delAllProduct = await Product.destroy({
        where: {},
        truncate: false,
        force: true
      })
      if (delAllProduct) {
        return res
        .status(200)
        .json(response("Success", "Sukses hapus semua data produk!"));
       
      } else {
        
        return res
        .status(400)
        .json(response("Failed", "Data produk tidak ada!"));
      }
    } catch (error) {
      return res.status(400).json(response("Failed", error.message, "Kosong"));
    }
  }

}

module.exports = ProductController;
