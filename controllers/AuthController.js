require('dotenv').config
const models = require("../models")
const {User} = require("../models")
const response = require("../helpers/response")
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt')

const {registerValidation} = require("../helpers/validation");
const jwtSecret = process.env.SECRET
const jwtSecretAdm = process.env.SECRET_ADMIN

class Controller{
    static async login(req,res){
        const {data} = req.body;
        console.log({data});
        try{
            const user = await models.User.findOne({
                where:{
                    username: req.body.data.username
                },
                raw:true,
            });

            const admin = await models.Admin.findOne({
                where:{
                    username: req.body.data.username
                },
                raw:true,
            });

            if(user){
                if (!bcrypt.compareSync(req.body.data.password, user.password)) {
                    return res
                      .status(422)
                      .json(response("Fail", "password incorrect"));
                }
                const token = jwt.sign(user.id, jwtSecret);
    
                delete user.password;
                delete user.salt;
                delete user.createdAt;
                delete user.updatedAt;
                return res
                  .status(200)
                  .json(response("Success", "Welcome!", { token, ...user }));
            }else if(admin){
                if (!bcrypt.compareSync(req.body.data.password, admin.password)) {
                    return res
                      .status(422)
                      .json(response("Fail", "password incorrect"));
                }
                const token = jwt.sign(admin.id, jwtSecretAdm);
    
                delete admin.password;
                delete admin.salt;
                delete admin.createdAt;
                delete admin.updatedAt;
                return res
                  .status(200)
                  .json(response("Success", "Welcome Admin", { token, ...admin }));
            }else{
              return res
              .status(422)
              .json(response("Fail", "Username or password incorrect"));
            }
        } catch(error){
            res.status(500).json(response("Fail", error.message));
        }
    }


    static async register(req, res) {
        const {
          firstName,
          lastName,
          email,
          phoneNumber,
          username,
          password
        } = req.body.data
    
        // Validasi user
        const {
          error
        } = registerValidation(req.body.data)
        if (error) return res.status(400).json(response("Failed!", error.details[0].message, ""))
    
        //cek data
        const emailExist = await models.User.findOne({
          where: {
            email: email
          }
        })
        const usernameExist = await models.User.findOne({
          where: {
            username: username
          }
        })
        const phoneNumberExist = await models.User.findOne({
            where: {
              phoneNumber: phoneNumber
            }
          })
    
        // Hash password
        const salt = bcrypt.genSaltSync(10);
        const hashedPassword = bcrypt.hashSync(password, salt)
    
        try {
          if (usernameExist) return res.status(404).json(response("Failed!", "Username sudah terdaftar!", ""))
          else if (emailExist) return res.status(404).json(response("Failed!", "Email sudah terdaftar!", ""))
          else if (phoneNumberExist) return res.status(404).json(response("Failed!", "Email sudah terdaftar!", ""))
          else {
            // simpan data user
            const savedUser = await User.create({
                firstName,
                lastName,
                email,
                phoneNumber,
                username,
                password: hashedPassword,
                salt,
                type:"user"
            })
    
            // tampilkan data yang ditambahkan
            const datas = {
                firstName: savedUser.firstName,
                lastName: savedUser.lastName,
                email: savedUser.email,
                phoneNumber: savedUser.phoneNumber,
                username: savedUser.username,
                type: savedUser.type
            }
    
            res.status(201).json(response("Success", "Berhasil tambah data user", datas))
          }
        } catch (error) {
          return res.status(500).json(response("Failed", error.message, ""))
        }
      }
}

module.exports = Controller;