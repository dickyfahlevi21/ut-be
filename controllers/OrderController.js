require('dotenv').config()

const response = require("../helpers/response");
const { Order, Order_Items, Payment, User, Admin, Product, Discount } = require("../models")
const midtransClient = require('midtrans-client');
const pagination = require("../helpers/pagination");

let core = new midtransClient.CoreApi({
    isProduction: false,
    serverKey: process.env.SERVER_KEY_MIDTRANS,
    clientKey: process.env.CLIENT_KEY_MIDTRANS
})


class OrderController{
    static async createOrder(req, res){
        try{
            const {idDiscount, courrierName, courrierService, courrierPrice, totalPayment, items, bank} = req.body.data;
            const idUser = req.user_id;
            const order = await Order.create({
                idUser,
                idDiscount,
                courrierName,
                courrierService,
                courrierPrice,
                totalPayment
            });

            if(!order){
                throw new Error(error.message)
            }
            items.forEach(async (val)=>{
                await Order_Items.create({
                    idOrder:order.id,
                    idProduct: val.idProduct,
                    qty: val.qty
                });
            });

            const user = await User.findByPk(req.user_id)
            const admin = await Admin.findByPk(req.body.data.items[0].idAdmin)

            let parameter = {
                "payment_type": "bank_transfer",
                
                "customer_details" : {
                    "first_name": user.firstName,
                    "last_name": user.lastName,
                    "email": user.email,
                    "phone": user.phoneNumber,
                },

                "transaction_details": {
                    "gross_amount": totalPayment,
                    "order_id":`order-${order.id}-${new Date().getTime()}`,
                },
                "bank_transfer":{
                    "bank": bank
                }
            };
            core.charge(parameter)
            
                .then( async (chargeResponse)=>{
                    const payment = await Payment.create({
                        idOrder: order.id,
                        token: chargeResponse.transaction_id,
                        idMidtrans: chargeResponse.order_id
                    })

                    return res 
                    .status (200)
                    .json(response ('Success', 'Data Order Created', chargeResponse))
                })
                .catch((e)=>{
                    console.log('Error occured:',e.message);
                })

        }catch(error){
            return res
            .status(500)
            .json(response('Failed', error.message))

        }
    }

    static async updateOrder(req, res){
        core.transaction.notification(req.body)
            .then(async (statusResponse)=>{
                    await Payment.update({
                        status: statusResponse.transaction_status
                    }, {
                        where: {
                            idMidtrans: statusResponse.order_id
                        }
                    })

                    return res
                        .status(200)
                        .json(response('Success', 'Order Updated Status', { statusResponse }))

                })
                .catch((error) => {
                    return res
                        .status(500)
                        .json(response('Fail', 'error occured', error.message))
                })
        }

        static async getOrder(req, res) {
            try {
                const { id } = req.params;
                const payment = await Payment.findByPk(id, {
                    include: [
                        {
                            model: Order,
                            include: [
                                { model: User },
                                { model: Discount },
                                {
                                    model: Order_Items,
                                    include: [{
                                        model: Product
                                    }]
                                }
                            ]
                        }
                    ]
                })
                
                if(!payment) {
                    return res
                    .status(400)
                    .json(response('Failed', `Data with id ${id} Not Found!`, payment))
                }

                return res
                    .status(200)
                    .json(response('Success', 'Data Found!', payment))
    
            } catch (error) {
                return res
                    .status(500)
                    .json(response('Fail', error.message))
            }
        }

        static async getAllOrder(req, res) {
            try {
                const count = await Payment.count()
    
                const page = pagination({
                    limit: req.query.limit,
                    page: parseInt(req.query.page),
                    count: count,
                })
    
                const getAll = await Payment.findAll({
                    limit: page.limit,
                    offset: page.offset,
                    include: [
                        {
                            model: Order,
                            include: [
                                { model: User },
                                { model: Discount },
                                {
                                    model: Order_Items,
                                    include: [{
                                        model: Product
                                    }]
                                }
                            ]
                        }
                    ]   
                })
    
                if (!getAll) {
                    throw new Error(error.message)
                }
    
                return res
                    .status(200)
                    .json(response('Success', 'Data Found!', {
                        data: getAll,
                        totalItems: page.totalItems,
                        totalPages: page.totalPages,
                        currentPage: page.currentPage,
                    }))
                
            } catch (error) {
                return res
                    .status(500)
                    .json(response('Fail', error.message))
            }
        }

        static async deleteOrder(req, res) {
            try {
                const { id } = req.params;
                const payment = await Payment.findByPk(id, {
                    include: [
                        {
                            model: Order,
                            include: [
                                { model: Order_Items }
                            ]
                        }
                    ]
                })

                const delPayment = await Payment.destroy({
                    where: {
                        id: payment.id
                    }
                })

                const delOrder = await Order.destroy({
                    where: {
                        id: payment.idOrder
                    }
                })

                const delOrderItems = await Order_Items.destroy({
                    where: {
                        idOrder: payment.idOrder
                    }
                })
                
                return res
                    .status(200)
                    .json(response('Success', 'Payment has been deleted', `ID : ${req.params.id}`))
    
            } catch (error) {
                return res
                    .status(500)
                    .json(response('Fail', error.message)   )
            }
        }
    
}


module.exports = OrderController;

