const { Product, Admin} = require('../models')
const response = require('../helpers/response')
const pagination = require('../helpers/pagination')
const Op = require('sequelize').Op

class SearchController{
    static async find(req,res){
        try{
            const count = await Product.findAndCountAll({
                where:{
                    [Op.or]:[
                        {
                            name:{
                                [Op.like] : `%${req.query.query}%`
                            }
                        },
                        {
                            description:{
                                [Op.like] : `%${req.query.query}%`
                            }
                        }
                    ]
                },
                include:[
                    {
                        model: Admin,
                        attributes:['id','username','email']
                    }
                ]
            })
            return res.status(200).json(response('Success','Search concluded',count))
        }catch(error){
            return res.status(500).json(response('Failed', error.message))
        }
    }
}

module.exports = SearchController