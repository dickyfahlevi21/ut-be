require("dotenv").config();
const response = require("../helpers/response");
const RajaOngkir = require("rajaongkir-nodejs").Starter(process.env.API_ONGKIR);


class OngkirController {
  static async getCityAll(req, res) {
    try {
      RajaOngkir.getCities().then(function (result){
        res.status(200).json(response("Success", "Berhasil tampil data", result));
      }).catch(function (err){
        return res.status(400).json(response("Failed", err));
      });
    } catch (err) {
      return res.status(400).json(response("Failed", err));
    }
  }

  static async getCost(req, res) {
    const { idDestination, idOrigin, weight, courier } = req.body.data
  
                  var params = {
                    origin: idOrigin, 
                    destination: idDestination, 
                    weight: 1700 
                };
                RajaOngkir.getTIKICost(params).then(function (result){
                  return res
                  .status(201)
                  .json(response("success","hasil",result));
                }).catch(function (error){
                  console.log('Error occured:',e.message);
              });
  }
}

module.exports = OngkirController;
