require('dotenv').config()
const { User } = require("../models")
const response = require('../helpers/response')
const attUser = ['id', 'firstName', 'lastName', 'email', 'phoneNumber' , 'username', 'type']

class UserController {
  // static async getUsers(req, res) {
  //   try {
  //     const count = await User.count()

  //     const page = pagination({
  //       limit: req.query.limit,
  //       page: parseInt(req.query.page),
  //       count: count
  //     })

  //     const show = await User.findAll({
  //       limit: page.limit,
  //       offset: page.offset,
  //       attributes: attUser
  //     })

  //     const datas = {
  //       data: show,
  //       totalItems: page.totalItems,
  //       totalPages: page.totalPages,
  //       currentPage: page.currentPage
  //     }
  //     res.status(200).json(response("Success", "Berhasil tampil data", datas))
  //   } catch (err) {
  //     return res.status(400).json(response("Failed", err.message, datas))
  //   }
  // }

  static async getUser(req, res) {
    const id = req.user_id

    const userget = await User.findByPk(
      id, {
        attributes: attUser
      }
    )

    try {
      if (userget) {
        return res.status(200).json(response("Success", "Data user ditemukan!", userget))
      } else {
        return res.status(400).json(response("Failed!", "Data user tidak ditemukan!", 'Kosong'))
      }
    } catch (error) {
      return res.status(404).json(response("Failed", error.message, 'Kosong!'))
    }
  }


  static async updateUser(req, res) {
      const id = req.user_id

      const {
          firstName,
          lastName,
          email,
          phoneNumber,
          username,
          password
      } = req.body.data;
  
      const userUpdate = await User.update({
          firstName,
          lastName,
          email,
          phoneNumber,
          username,
          password
      }, {
        where: {
          id
        }
      })
  
      const showUser = await User.findByPk(
        id, {
          attributes: attUser
        }
      )

      try {
        //   if (password) {
        //     return res.status(400).json(response("Failed", "Update password hanya bisa oleh Admin!", "Kosong"))
        //   } 
        if (userUpdate) {
          return res.status(200).json(response("Success", "Sukses update user!", showUser))
        } else {
          return res.status(400).json(response("Failed!", "Data user tidak ada!", "Kosong"))
        }
      } catch (error) {
        return res.status(400).json(response("Failed", error.message, "Kosong"))
      }
  }

  // static async deleteUser(req, res) {
  //   const {
  //     id
  //   } = req.params

  //   const delUser = await User.destroy({
  //     where: {
  //       id: id
  //     }
  //   })

  //   try {
  //     if (delUser) {
  //       return res.status(200).json(response("Success", "Sukses hapus data user!", `ID : ${id}`))
  //     } else {
  //       return res.status(400).json(response("Failed", "Data tidak user tidak ada!", `ID : ${id}`))
  //     }
  //   } catch (error) {
  //     return res.status(400).json(response("Failed", error.message, "Kosong"))
  //   }
  // }
}

module.exports = UserController