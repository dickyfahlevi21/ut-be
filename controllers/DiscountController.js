require("dotenv").config();
const { Discount, Order } = require("../models");
const response = require("../helpers/response");
const pagination = require("../helpers/pagination");

const attDiscount = ["id", "discountName", "expiredTime", "totalDiscount"];

class DiscountController {
	static async saveDiscount(req, res) {
		try {
			if (req.admin_id) {
				const { discountName, expiredTime, totalDiscount } = req.body.data;

				const saveDiscount = await Discount.create({
					discountName,
					expiredTime,
					totalDiscount,
				});
				const datas = {
					discountName: saveDiscount.discountName,
					expiredTime: saveDiscount.expiredTime,
					totalDiscount: saveDiscount.totalDiscount,
				};
				res
					.status(201)
					.json(response("success", "berhasil tambah data discount", datas));
			} else {
				res.status(404).json(response("Failed", "You don't have access!"));
			}
		} catch (error) {
			res.status(404).json(response("Failed", error.message));
		}
	}

	static async getDiscounts(req, res) {
		try {
			if (req.admin_id) {
				const count = await Discount.count();

				const page = pagination({
					limit: req.query.limit,
					page: parseInt(req.query.page),
					count: count,
				});

				const show = await Discount.findAll({
					limit: page.limit,
					offset: page.offset,
					attributes: attDiscount,
					include: [
						{
							model: Order,
						},
					],
				});

				const datas = {
					data: show,
					totalItems: page.totalItems,
					totalPages: page.totalPages,
					currentPages: page.currentPage,
				};
				return res
					.status(200)
					.json(response("Success", "Berhasil menampilkan data", datas));
			}
			if (req.user_id) {
                return res
					.status(404)
					.json(response("Restricted", "Anda bukan admin", null));
			}
		} catch (error) {
			return res.status(400).json(response("failed", error.message));
		}
	}

	static async deleteDiscount(req, res) {
		const { id } = req.params;
		const delDiscount = await Discount.destroy({
			where: {
				id: id,
			},
		});

		try {
			if (delDiscount) {
				return res
					.status(200)
					.json(
						response("Success", "Sukses hapus data discount!", `ID : ${id}`)
					);
			} else {
				return res
					.status(400)
					.json(response("Failed", "Data discount tidak ada!", `ID : ${id}`));
			}
		} catch (error) {
			return res.status(400).json(response("Failed", error.message, "Kosong"));
		}
	}

	static async deleteDiscountAll(req, res) {
		try {
			const delAllDiscount = await Discount.destroy({
				where: {},
				truncate: false,
				force: true,
			});
			if (delAllDiscount) {
				return res
					.status(200)
					.json(response("Success", "Sukses hapus semua data discount!"));
			} else {
				return res
					.status(400)
					.json(response("Failed", "Data discount tidak ada!"));
			}
		} catch (error) {
			return res.status(400).json(response("Failed", error.message, "Kosong"));
		}
	}
}

module.exports = DiscountController;
