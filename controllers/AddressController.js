require('dotenv').config()
const { Address, User, Admin } = require("../models")
const response = require('../helpers/response')
const pagination = require("../helpers/pagination")
const { Op } = require("sequelize");

const attAddress = ['id', 'idUser', 'idAdmin', 'addressName', 'idCity', 'city', 'zipCode', 'address']
const attUser = ['id', 'firstName', 'lastName', 'email', 'phoneNumber' , 'username', 'type']
const attAdmin = ['id', 'firstName', 'lastName', 'email', 'phoneNumber' , 'username', 'type']
class AddressController {
	static async getAddressAll(req, res) {
		try {
			const count = await Address.count();

			const page = pagination({
				limit: req.query.limit,
				page: parseInt(req.query.page),
				count: count,
			});

			if (req.admin_id) {
				const show = await Address.findAll({
					limit: page.limit,
					offset: page.offset,
					attributes: attAddress,
					where: {
						idAdmin: {
							[Op.not]: null,
						},
					}
				});

				const datas = {
					data: show,
					totalItems: page.totalItems,
					totalPages: page.totalPages,
					currentPage: page.currentPage,
				};

				return res
					.status(200)
					.json(response("Success", "Berhasil tampil data", datas));
			}

			if (req.user_id) {
				const show = await Address.findAll({
					limit: page.limit,
					offset: page.offset,
					attributes: attAddress,
					where: {
						idUser: {
							[Op.not]: null,
						},
					},
					include: [
						{
							model: User,
							attributes: attUser,
						},
						{
							model: Admin,
							attributes: attAdmin,
						},
					],
				});

				const datas = {
					data: show,
					totalItems: page.totalItems,
					totalPages: page.totalPages,
					currentPage: page.currentPage,
				};

				return res
					.status(200)
					.json(response("Success", "Berhasil tampil data", datas));
			}
		} catch (err) {
			return res.status(400).json(response("Failed", err));
		}
	}

	static async getAddress(req, res) {
		const { id } = req.params;

		const addressGet = await Address.findByPk(id, {
			attributes: attAddress,
			include: [
				{
					model: User,
					attributes: attUser,
				},
				{
					model: Admin,
					attributes: attAdmin,
				},
			],
		});

		try {
			if (addressGet) {
				return res
					.status(200)
					.json(response("Success", "Data address ditemukan!", addressGet));
			} else {
				return res
					.status(400)
					.json(response("Failed!", "Data address tidak ditemukan!", ""));
			}
		} catch (error) {
			return res.status(404).json(response("Failed", error.message, ""));
		}
	}

	static async saveAddress(req, res) {
		try {
			const { addressName, idCity, city, zipCode, address } = req.body.data;
			const idUser = req.user_id;
			const idAdmin = req.admin_id;

			if (idUser) {
				// tambah address
				const saveAddress = await Address.create({
					idUser,
					addressName,
					idCity,
					city,
					zipCode,
					address,
				});

				return res
					.status(201)
					.json(response("Success", "Berhasil tambah data address", saveAddress));
			}

			if (idAdmin) {
				return res
					.status(201)
					.json(response("Success", "Maaf, admin hanya punya 1 alamat", null));
			}
		} catch (error) {
			return res.status(404).json(response("Failed", error.message));
		}
	}

	static async updateAddress(req, res) {
		try {
			const { id } = req.params;
			const { addressName, idCity, city, zipCode, address } = req.body.data;

			const updateAddress = await Address.update(
				{
					addressName,
					idCity,
					city,
					zipCode,
					address,
				},
				{
					where: {
						id: id,
					},
				}
			);

			const showAddress = await Address.findByPk(id, {
				attributes: attAddress,
			});

			if (updateAddress) {
				return res
					.status(200)
					.json(response("Success", "Sukses update data address!", showAddress));
			} else {
				return res
				.status(400)
				.json(response("Failed!", "Data address tidak ada!", null));
			}
		} catch (error) {
			res.status(404).json(response("Failed", error.message));
		}
	}

	static async deleteAddress(req, res) {
		try {
			if (req.user_id) {
				const { id } = req.params;

				const delAddress = await Address.destroy({
					where: {
						id: id,
					},
				});
				if (delAddress) {
					return res
						.status(200)
						.json(response("Success", "Sukses hapus data address!", `ID : ${id}`)
						);
				} else {
					return res
						.status(400)
						.json(response("Failed", "Data address tidak ada!", `ID : ${id}`));
				}
			}
		} catch (error) {
			return res.status(400).json(response("Failed", error.message, "Kosong"));
		}
	}
}

module.exports = AddressController;
