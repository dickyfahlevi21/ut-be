require('dotenv').config()
const jwt = require('jsonwebtoken')
const response = require('../helpers/response')

module.exports = async (req, res, next) => {
    if (!req.headers['authorization']) res.status(401).json(response('fail', 'unauthenticated'))

    const token = req.headers['authorization'].split(' ')[1]
    const admin = process.env.SECRET_ADMIN
    const user = process.env.SECRET
    try {
        jwt.verify(token, user, function(err, res) {
            if(err){
                return true
            }
            req.token = token
            req.user_id = jwt.decode(token, user)
            return next()
        })
        jwt.verify(token, admin, function(err, res) {
            if (err) {
                return false
            }
            req.token = token
            req.admin_id = jwt.decode(token, admin)
            return next()
        })
    } catch (error) {
        return res.status(500).json(response('Fail', error.message))
    }
}