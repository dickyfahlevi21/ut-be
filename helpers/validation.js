const Joi = require('@hapi/joi')

// register validation
const registerValidation = (data) => {
    const schema = Joi.object({
        firstName: Joi.string().min(3).required(),
        lastName: Joi.string().optional(),
        email: Joi.string()
            .min(6)
            .email()
            .required(),
        phoneNumber: Joi.string().min(10).required(),
        username: Joi.string().min(6).required(),
        password: Joi.string().min(6).required(),
        role: Joi.string().optional()
    });
    return schema.validate(data);
}

// login validation
const loginValidation = (data) => {
    const schema = Joi.object({
        username: Joi.string().min(6).required(),
        password: Joi.string().min(6).required()
    });
    return schema.validate(data);
}

module.exports.registerValidation = registerValidation;
module.exports.loginValidation = loginValidation;