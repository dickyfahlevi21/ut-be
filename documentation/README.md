# Backend Simple WMS

## Mockups Design:

[FIGMA](https://www.figma.com/file/xZU0wwbhsbLdGLpCO6QFCP/Vuex?node-id=19%3A48)

## Api Documentation

-
-
-

### Sign Up
#### Success

![SIGN UP](./img/register.png)

#### Failed Example

![SIGN UP](./img/register-fail.png)

### Login
#### Success
##### Admin
![LOGIN](./img/login-admin.png)
##### User
![LOGIN](./img/login-user.png)

#### Failed

![LOGIN](./img/login-fail.png)

-
-
-

# GET

### Get All Product

![Get All Product](./img/product-get-all.png)

### Get Product by ID

![Get All Product](./img/product-get-id.PNG)

-

## Note: Untuk Endpoint dibawah ini, butuh 'token' Authorization dari login

-

# Get

### Get User By id
![Get User By id](./img/user-get-id.png)

### Get Discount All (Admin)
![Get Discount All](./img/discount-get-all.png)

### Get Address by ID
![Get Address By id](./img/address-get-id.png)

### Get Address All
![Get Address All](./img/address-get-all.png)

### Get Order All
![Get Order All](./img/order-get-all.png)

### Get Order by ID
![Get Order by ID](./img/order-get-id.png)

-
-
-

# POST

### POST Product
![Post Product](./img/product-post.png)

### POST Discount
![Post Discount](./img/discount-post.png)

### POST Address
![Post Discount](./img/address-post.png)

### POST Order
![Post Order](./img/order-post.png)

-
-
-

# PUT

### PUT - Update Product By Id
![PUT - Update Product By Id](./img/product-put.png)

### PUT - Update Address By Id
![PUT - Update Address By Id](./img/address-put-id.png)

-
-
-

# DELETE

### DELETE Product By id
![DELETE Product By id](./img/product-del-id.png)

### DELETE Product All
![DELETE Pruduct All](./img/product-del-all.png)

### DELETE Discount by ID
![DELETE Discount by ID](./img/discount-del-id.png)

### DELETE Discount All
![DELETE Discount All](./img/discount-del-all.png)

### DELETE Address by ID
![DELETE Address by ID](./img/address-del-id.png)

### DELETE Payment and Order by ID
![DELETE Payment and Order by ID](./img/order-payment-del-id.png)

-
-
-