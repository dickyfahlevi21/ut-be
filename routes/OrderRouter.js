const express = require('express')
const router = express.Router()
const auth = require("../middleware/AuthMiddleware");
const OrderController = require('../controllers/OrderController')

router
  .post('/', auth, OrderController.createOrder)
  .post('/webhook', OrderController.updateOrder)
  .get('/', OrderController.getAllOrder)
  .get('/:id', OrderController.getOrder)
  .delete('/:id', OrderController.deleteOrder)

module.exports = router