const express = require('express')
const router = express.Router()
const auth = require("../middleware/AuthMiddleware");

const ProductController = require('../controllers/ProductController')

router
  .get('/home', ProductController.home)
  .get('/', ProductController.getProducts)
  .get('/:id', ProductController.getProduct)
  .post('/', auth, ProductController.saveProduct)
  .put('/:id', auth, ProductController.updateProduct)
  .delete('/del', auth, ProductController.deleteProductAll)
  .delete('/:id', auth, ProductController.deleteProduct)
  

module.exports = router