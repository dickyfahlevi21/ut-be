const express = require('express')
const router = express.Router()

const OngkirController = require('../controllers/OngkirController')

router
  .get('/', OngkirController.getCityAll)
  .post('/', OngkirController.getCost)

module.exports = router