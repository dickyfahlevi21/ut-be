const express = require('express')
const router = express.Router()

const AddressController = require('../controllers/AddressController')

router
  .get('/', AddressController.getAddressAll)
  .get('/:id', AddressController.getAddress)
  .post('/', AddressController.saveAddress)
  .put('/:id', AddressController.updateAddress)
  .delete('/:id', AddressController.deleteAddress)

module.exports = router