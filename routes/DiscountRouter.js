const express = require('express')
const router = express.Router()

const DiscountController = require('../controllers/DiscountController')

router
    .get('/', DiscountController.getDiscounts)
    .post('/', DiscountController.saveDiscount)
    .delete('/del', DiscountController.deleteDiscountAll)
    .delete('/:id', DiscountController.deleteDiscount)

module.exports = router