require("dotenv").config();

const express = require("express");
const cors = require("cors");
const fileUpload = require("express-fileupload");
// const router = express.Router()

const app = express();

const port = process.env.PORT || 3000;


const auth = require("./middleware/AuthMiddleware");
const userRoute = require("./routes/UserRouter");
const productRoute = require("./routes/ProductRouter");
const discountRoute = require("./routes/DiscountRouter");
const authRoute = require("./routes/auth");
const addressRoute = require("./routes/AddressRouter");
const ongkirRoute = require("./routes/OngkirRouter");
const orderRoute = require("./routes/OrderRouter");
const searchRoute = require('./routes/SearchRouter');
// app.use(function (req, res, next) {
//   res.header("Access-Control-Allow-Origin", "*");
//   res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
//   next();
// });


app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Methods", "GET, PUT, POST, DELETE, PATCH");
  res.header("Access-Control-Allow-Headers", "Accept, Content-Type, Authorization, X-Requested-With");

  next();
});

//cors
app.use(cors());


// untuk cloudinary
app.use(
  fileUpload({
    useTempFiles: true,
  })
);

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

app.get("/", (req, res) => {
  res.json({ hello: "world" });
});

app.use("/api/v1/user", auth, userRoute);
app.use("/api/v1/product", productRoute);
app.use("/api/v1/auth", authRoute);
app.use("/api/v1/address", auth, addressRoute);
app.use("/api/v1/disc", auth, discountRoute);
app.use("/api/v1/ongkos", ongkirRoute);
app.use("/api/v1/order", orderRoute);
app.use("/api/v1/search", searchRoute);


app.listen(port, () => console.log("Listened on port " + port));
