'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order_Items extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Order,{foreignKey:"idOrder"})
      this.belongsTo(models.Product,{foreignKey:"idProduct"})
    }
  };
  Order_Items.init({
    idOrder: DataTypes.INTEGER,
    idProduct: DataTypes.INTEGER,
    qty: DataTypes.INTEGER,
  }, {
    sequelize,
    modelName: 'Order_Items',
  });
  return Order_Items;
};