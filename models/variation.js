'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Variation extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Product, { foreignKey: "idProduct"})
    }
  };
  Variation.init({
    color: DataTypes.STRING,
    size: DataTypes.STRING,
    material: DataTypes.STRING,
    idProduct: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Variation',
  });
  return Variation;
};