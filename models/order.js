'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Order extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasOne(models.Payment, { foreignKey: 'idOrder' })
      this.belongsTo(models.Discount, { foreignKey: 'idDiscount' })
      this.hasMany(models.Order_Items, { foreignKey: 'idOrder' })
      this.belongsTo(models.User, { foreignKey: 'idUser' })
    }
  };
  Order.init({
    idUser: DataTypes.INTEGER,
    idDiscount: DataTypes.INTEGER,
    courrierName: DataTypes.STRING,
    courrierService: DataTypes.STRING,
    courrierPrice: DataTypes.INTEGER,
    totalPayment: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Order',
  });
  return Order;
};