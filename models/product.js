'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Admin, { foreignKey: "idAdmin"})
      this.hasOne(models.Variation, { foreignKey: "idProduct"})
      this.hasOne(models.Category, { foreignKey: "idProduct"})
      this.hasOne(models.Order_Items, { foreignKey: "idProduct"})
    }
  };
  Product.init({
    idAdmin: DataTypes.INTEGER,
    name: DataTypes.STRING,
    price: DataTypes.INTEGER,
    weight: DataTypes.INTEGER,
    description: DataTypes.STRING,
    tags: DataTypes.STRING,
    stock: DataTypes.INTEGER,
    picture: DataTypes.STRING,
    publicId: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};