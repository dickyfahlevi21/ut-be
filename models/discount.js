'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Discount extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.hasMany(models.Order,{foreignKey: "idDiscount"})
    }
  };
  Discount.init({
    discountName: DataTypes.STRING,
    expiredTime: DataTypes.DATE,
    totalDiscount: DataTypes.FLOAT
  }, {
    sequelize,
    modelName: 'Discount',
  });
  return Discount;
};