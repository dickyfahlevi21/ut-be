'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Address extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      this.belongsTo(models.Admin, { foreignKey: "idAdmin" })
      this.belongsTo(models.User, { foreignKey: "idUser"})
    }
  };
  Address.init({
    idUser: DataTypes.INTEGER,
    idAdmin: DataTypes.INTEGER,
    addressName: DataTypes.STRING,
    idCity: DataTypes.INTEGER,
    city: DataTypes.STRING,
    zipCode: DataTypes.INTEGER,
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Address',
  });
  return Address;
};