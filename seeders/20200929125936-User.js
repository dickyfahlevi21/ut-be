'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Users", [
      {
      firstName: "panca",
      lastName: "last",
      email:"panca@gmail",
      phoneNumber:"0897683902186",
      username:"pancax",
      password:"$2b$10$yD.uA5elNvUyhqwosMr7senoIzRdd6m3fD5WcK/SRFWY0JuFpvcB.",
      salt:"$2b$10$yD.uA5elNvUyhqwosMr7se",
      type:"user",
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])   
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("Users", null, {})
  }
};
