'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Products", [
      {
      idAdmin:1,
      name:"Bagsssssss",
      price:200000,
      weight: 10,
      description:"test des",
      tags:"tags tes",
      stock: 10,
      publicId: 2352352352,
      picture:"picture.url",
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])   
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Products", null, {})
  }
};
