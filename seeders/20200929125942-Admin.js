'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Admins", [
      {
      firstName: "admin1",
      lastName: "admin1",
      email:"rtx@gmail",
      phoneNumber:"082183838844",
      username:"admin1",
      password:"$2b$10$AQwoSQF0j55A9BK6Nfq91uteqKAxGygauqdpsx2hwwk4yIpgWsGpq",
      salt:"$2b$10$AQwoSQF0j55A9BK6Nfq91u",
      type:"admin",
      createdAt: new Date(),
      updatedAt: new Date()
    }
  ])   
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    return queryInterface.bulkDelete("Admins", null, {})
  }
};
